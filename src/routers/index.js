// Containers 
import AuthContainer from '@containers/Auth_container/index'
import MainContainer from '@containers/Main_container/index'
import EmptyContiner from '@containers/Empty_container/index'
// pages
import P404 from '@pages/exceptions/PageNotFound'
import Dashboard from '@pages/dashboard/index'
import Login from '@pages/login'

export const routes = [
    {
        path: '/*',
        title: 'dashboard',
        hideChildren: false,
        component: MainContainer,
        children: [
            {
                path: 'main',
                title: 'Dashboard',
                component: Dashboard,
                children: []
            }
        ]
    },
    {
        path: '/login',
        title: 'login',
        hideChildren: false,
        component: AuthContainer,
        children: [
            {
                path: 'auth',
                title: 'auth',
                component: Login,
                children: []
            }
        ]
    },
    {
        path: '/exception',
        title: 'exception',
        hideChildren: false,
        component: EmptyContiner,
        children: [
            {
                path: '404',
                title: 'Not found',
                component: P404,
                children: []
            }
        ]
    }

]